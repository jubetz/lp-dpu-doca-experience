# Next Steps

Congratulations!

You have successfully completed the DPU Experience. 

NVIDIA AI Enterprise software suite includes the frameworks and tools needed for best-in-class AI, the same ones you just used in your lab. And since NVIDIA has done the integration and performance optimizations, you can get started with AI easier and faster.

What's next?

1. To continue your LaunchPad experience, contact your NVIDIA Account Manager, who was included in your welcome email.
2. Do you have a team member who would also benefit from LaunchPad? They can fill out [this form](https://nvidia.com/launchpad) to get access.
3. Ready to start your AI journey? Contact your NVIDIA Account rep or NVIDIA Partner. You will need:
    * An NVIDIA AI Enterprise compatible server
    * NVIDIA AI Enterprise software
    * VMware vSphere 7 U2 or later
4. Need more information? Here are some resources to learn more:
    * [Technical documentation, including Deployment Guides and Reference Architectures](https://resources.nvidia.com/en-us-nvaie-resource-center/en-us-nvaie/ai-enterprise?lb-mode=preview).
    * [NVIDIA AI Enterprise Packaging, Licensing and Pricing Guide](https://resources.nvidia.com/en-us-nvaie-resource-center/en-us-nvaie/nvidia-ai-enterprise-licensing-pg?lb-mode=preview).
    * [NVIDIA AI Enterprise Support Options](https://resources.nvidia.com/en-us-nvaie-resource-center/en-us-nvaie/support-services-brief-nvidia-ai?lb-mode=preview).
    * [90-day NVIDIA AI Enterprise trial](https://enterpriseproductregistration.nvidia.com/?LicType=EVAL&ProductFamily=NVAIEnterprise).